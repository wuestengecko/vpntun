from importlib import metadata

try:
    __version__ = metadata.version("private-pc-support")
except metadata.PackageNotFoundError:
    __version__ = "0.0.0+unknown"
del metadata
