from __future__ import annotations

import logging
import os
from typing import TYPE_CHECKING, no_type_check

import anyio
from dbus_next import PropertyAccess
from dbus_next.aio import MessageBus
from dbus_next.service import ServiceInterface, dbus_property, signal

if TYPE_CHECKING:
    from .__main__ import ReconnectCallback

logger = logging.getLogger(__name__)

SNI = "org.kde.StatusNotifierItem"
SNW = "org.kde.StatusNotifierWatcher"


class TrayIcon(ServiceInterface):
    def __init__(self) -> None:
        super().__init__(SNI)
        self.__conn: asyncssh.SSHClientConnection | None = None

    async def on_reconnect(
        self, conn: asyncssh.SSHClientConnection | None
    ) -> None:
        self.__conn = conn
        self.NewIcon()
        self.NewToolTip()

    def __repr__(self) -> str:
        return f"<{self.__class__.__name__} {self.__name!r}>"

    @dbus_property(PropertyAccess.READ)
    @no_type_check
    def Category(self) -> "s":
        return "ApplicationStatus"

    @dbus_property(PropertyAccess.READ)
    @no_type_check
    def Id(self) -> "s":
        return "vpntun"

    @dbus_property(PropertyAccess.READ)
    @no_type_check
    def Title(self) -> "s":
        return "VPN Tunnel"

    @dbus_property(PropertyAccess.READ)
    @no_type_check
    def Status(self) -> "s":
        return "Active"

    @dbus_property(PropertyAccess.READ)
    @no_type_check
    def WindowId(self) -> "u":
        return 0

    @dbus_property(PropertyAccess.READ)
    @no_type_check
    def IconName(self) -> "s":
        if self.__conn is not None:
            return "laptopconnected"
        else:
            return "laptopdisconnected"

    @dbus_property(PropertyAccess.READ)
    @no_type_check
    def ToolTip(self) -> "(sa(iiay)ss)":
        name = (
            "laptopconnected"
            if self.__conn is not None
            else "laptopdisconnected"
        )
        title = "VPN Tunnel"
        description = (
            "Connected" if self.__conn is not None else "Disconnected"
        )
        return [name, [], title, description]

    @dbus_property(PropertyAccess.READ)
    @no_type_check
    def ItemIsMenu(self) -> "b":
        return False

    @signal()
    @no_type_check
    def NewIcon(self):
        pass

    @signal()
    @no_type_check
    def NewToolTip(self):
        pass


async def run(
    *,
    task_status: anyio.TaskStatus[ReconnectCallback],
) -> None:
    bus = await MessageBus().connect()
    try:
        icon = TrayIcon()
        await _register_icon(bus, icon, f"{SNI}-{os.getpid()}-1")

        task_status.started(icon.on_reconnect)
        await bus.wait_for_disconnect()
        logger.error("DBus connection closed unexpectedly")
    finally:
        logger.info("Closing DBus connection for tray icon")
        bus.disconnect()


async def _register_icon(bus: MessageBus, icon: TrayIcon, name: str) -> None:
    bus.export("/StatusNotifierItem", icon)
    await bus.request_name(name)
    intro = await bus.introspect(SNW, "/StatusNotifierWatcher")
    watcher_proxy = bus.get_proxy_object(SNW, "/StatusNotifierWatcher", intro)
    watcher = watcher_proxy.get_interface(SNW)
    await watcher.call_register_status_notifier_item(name)  # type: ignore[attr-defined]
