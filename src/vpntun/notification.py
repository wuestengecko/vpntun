from __future__ import annotations

import logging
from typing import TYPE_CHECKING, no_type_check

import anyio
from dbus_next import Variant
from dbus_next.aio import MessageBus
from dbus_next.aio.proxy_object import ProxyInterface

if TYPE_CHECKING:
    from .__main__ import ReconnectCallback

logger = logging.getLogger(__name__)


class Notifier:
    def __init__(self, server_iface: ProxyInterface) -> None:
        self.__id = 0
        self.__server = server_iface
        server_iface.on_notification_closed(  # type: ignore[attr-defined]
            self.on_notification_closed
        )

    async def on_reconnect(
        self, conn: asyncssh.SSHClientConnection | None
    ) -> None:
        appname = "VPN Tunnel"
        body = ""
        actions: list = []
        hints = {"urgency": Variant("y", 1)}
        timeout = 5000
        if conn is not None:
            icon = "laptopconnected"
            summary = "Tunnel connected"
        else:
            icon = "laptopdisconnected"
            summary = "Tunnel connection lost"
        self.__id = await self.__server.call_notify(  # type: ignore[attr-defined]
            appname, self.__id, icon, summary, body, actions, hints, timeout
        )
        logger.debug("Notification sent, ID=%d", self.__id)

    @no_type_check
    async def on_notification_closed(self, _id: "u", _reason: "u") -> None:
        logger.debug("Notification was closed")
        self.__id = 0

    async def on_shutdown(self) -> None:
        if self.__id != 0:
            logger.debug("Clearing notification")
            await self.__server.call_close_notification(  # type: ignore[attr-defined]
                self.__id
            )


async def run(
    *,
    task_status: anyio.TaskStatus[ReconnectCallback],
) -> None:
    bus = await MessageBus().connect()
    try:
        intro = await bus.introspect(
            "org.freedesktop.Notifications", "/org/freedesktop/Notifications"
        )
        proxy = bus.get_proxy_object(
            "org.freedesktop.Notifications",
            "/org/freedesktop/Notifications",
            intro,
        )
        iface = proxy.get_interface("org.freedesktop.Notifications")
        notifier = Notifier(iface)
        task_status.started(notifier.on_reconnect)

        await bus.wait_for_disconnect()
        logger.error("DBus connection closed unexpectedly")
    finally:
        if bus.connected:
            await notifier.on_shutdown()
        logger.info("Closing DBus connection for notifications")
        bus.disconnect()
