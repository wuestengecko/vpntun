"""The DB VPN tunnel."""

from __future__ import annotations

import asyncio
import collections.abc as cabc
import dataclasses
import functools
import importlib
import logging
import pathlib
import shlex
import socket
import time

import anyio
import appdirs as _appdirs  # type: ignore[import]
import asyncclick as click  # type: ignore[import]
import asyncssh  # type: ignore[import]
from typing_extensions import TypeAlias

import vpntun

logger = logging.getLogger(__name__)

try:
    import uvloop
except ImportError:
    logger.warning("uvloop not available, consider installing it")
else:
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

appdirs = _appdirs.AppDirs("DB VPN Tunnel", "SE Toolchain")
machine_key = pathlib.Path(appdirs.user_data_dir, "machine.key")
foreign_keyring = pathlib.Path(appdirs.user_data_dir, "foreign.key")

_click_port = click.IntRange(1, 2**16 - 1)
_default_ssh_port = 23160
_default_socks_port = 23161
_max_connect_retry = 60

ReconnectCallback: TypeAlias = cabc.Callable[
    ["asyncssh.SSHClientConnection | None"],
    cabc.Awaitable[None],
]


class ConnectionTracker:
    def __init__(self, task_group: anyio.abc.TaskGroup) -> None:
        self.__tg = task_group
        self.__conn: asyncssh.SSHClientConnection | None = None
        self.__callbacks: list[ReconnectCallback] = []

    @property
    def connection(self) -> asyncssh.SSHClientConnection | None:
        return self.__conn

    def reconnect(self, conn: asyncssh.SSHClientConnection | None) -> None:
        self.__conn = conn
        for cb in self.__callbacks:
            self.__tg.start_soon(cb, conn)

    def add_callback(self, cb: ReconnectCallback) -> None:
        self.__callbacks.append(cb)
        if self.__conn is not None:
            cb(self.__conn)


class LocalForwardParam(click.ParamType):
    name = "[[LISTEN_PORT:][DEST_HOST]:]DEST_PORT"

    __port_min = 1
    __port_max = 2**16 - 1

    def convert(self, value, param, ctx):
        if not isinstance(value, str):
            return value
        parts = value.split(":")
        if not 1 <= len(parts) <= 4:
            self.fail("Local forward spec must have 1 to 3 parts")
        try:
            dest_port = int(parts[-1])
        except ValueError:
            self.fail(f"DEST_PORT must be an integer: {parts[-1]}")
        if not self.__port_min <= dest_port <= self.__port_max:
            raise ValueError(f"DEST_PORT out of range: {dest_port}")

        try:
            dest_host = parts[-2] or "localhost"
        except IndexError:
            dest_host = "localhost"

        try:
            listen_port = int(parts[-3])
        except ValueError:
            self.fail(f"LISTEN_PORT must be an int between 1 and {2**16-1}")
        except IndexError:
            listen_port = dest_port
        if not self.__port_min <= dest_port <= self.__port_max:
            raise ValueError(f"LISTEN_PORT out of range: {listen_port}")

        try:
            listen_host = parts[-4] or "localhost"
        except IndexError:
            listen_host = "localhost"

        return ForwardSpec(listen_host, listen_port, dest_host, dest_port)


@dataclasses.dataclass(frozen=True)
class ForwardSpec:
    listen_host: str
    listen_port: int
    dest_host: str
    dest_port: int


@click.group()
@click.option("-v", "--verbose", count=True)
@click.option("-q", "--quiet", count=True)
@click.version_option(vpntun.__version__, "-V", "--version")
def main(verbose: int, quiet: int) -> None:
    """The DB VPN tunnel."""
    logging.basicConfig(
        format="<%(name)s> %(levelname)s: %(message)s",
        level=max(1, logging.INFO + 10 * (quiet - verbose)),
    )


@main.command()
@click.argument("ip")
@click.argument("port", type=_click_port, default=_default_ssh_port)
async def client(*, ip: str, port: int) -> None:
    """The DB VPN tunnel client."""
    _ensure_foreign_keys()
    logger.info("Press CTRL-C to stop the client")
    retry = 0
    while (retry := retry + 1) <= _max_connect_retry:
        logger.info(
            "Connecting to %s:%d (%d/%d)", ip, port, retry, _max_connect_retry
        )
        try:
            open_time = time.time()
            connection = await asyncssh.connect_reverse(
                ip,
                port,
                server_factory=_ReverseServer,
                server_host_keys=[_load_or_generate_machine_key()],
            )
            retry = 0
            connection.set_keepalive(5, 3)
            await connection.wait_closed()
        except asyncssh.ConnectionLost:
            logger.warning("Connection lost, reconnecting...")
        except (asyncssh.PermissionDenied, OSError) as err:
            logger.critical("Server refused connection: %s", err)
        except asyncssh.DisconnectError as err:
            logger.error("Unexpected disconnect, will retry: %s", err.reason)
        except (KeyboardInterrupt, anyio.get_cancelled_exc_class()):
            break
        else:
            logger.info("Connection closed, reconnecting...")
        cooldown = time.time() - open_time < 5
        await anyio.sleep(5 * bool(retry or cooldown))
    logger.info("Bye!")


@main.command()
@click.argument("port", type=_click_port, default=_default_ssh_port)
@click.option("--proxy-listen", default="127.0.0.1")
@click.option("--proxy-port", type=_click_port, default=_default_socks_port)
@click.option("-L", "forward", type=LocalForwardParam(), multiple=True)
@click.option("--exec-connect", default=None)
async def server(
    *,
    port: int,
    proxy_listen: str,
    proxy_port=int,
    forward: cabc.Iterable[ForwardSpec],
    exec_connect: str | None,
) -> None:
    """The DB VPN tunnel server."""
    _ensure_foreign_keys()
    logger.info("Press CTRL-C to stop the server")
    async with anyio.create_task_group() as tg:
        tracker = ConnectionTracker(tg)
        async with await asyncssh.listen_reverse(
            port=port,
            reuse_address=True,
            reuse_port=True,
            client_factory=functools.partial(_ReverseClient, tracker),
        ):
            cb: ReconnectCallback = functools.partial(
                _open_proxy,
                listen=proxy_listen,
                port=proxy_port,
                local_forward=forward,
            )
            tracker.add_callback(cb)

            if exec_connect:
                tracker.add_callback(ExecCallback(exec_connect).on_reconnect)

            for module, feature in (
                ("vpntun.tray", "tray icon"),
                ("vpntun.notification", "desktop notifications"),
            ):
                try:
                    mod = importlib.import_module(module)
                    cb = await tg.start(mod.run)
                except ImportError as err:
                    logger.warning(
                        "Missing dependencies for %s: %s", feature, err
                    )
                except Exception:
                    logger.exception("Error enabling %s", feature)
                else:
                    tracker.add_callback(cb)

            await anyio.sleep_forever()


@main.command()
@click.option("--remove-known-keys", is_flag=True)
async def exchange(*, remove_known_keys: bool) -> None:
    """Exchange keys with a new machine."""
    if remove_known_keys:
        try:
            foreign_keyring.unlink()
        except FileNotFoundError:
            logger.info("Keyring is already empty")
        else:
            logger.info("Removed existing keyring")

    _exchange_foreign_key()


def _load_or_generate_machine_key() -> asyncssh.SSHKey:
    try:
        return asyncssh.read_private_key(machine_key)
    except FileNotFoundError:
        machine_key.parent.mkdir(exist_ok=True, parents=True)
        logger.info("Generating machine key, this may take a while...")
        key = asyncssh.generate_private_key(
            "ssh-ed25519", f"vpntun key for {socket.getfqdn()}"
        )
        key.write_private_key(machine_key)
        return key


def _ensure_foreign_keys() -> None:
    try:
        asyncssh.read_public_key_list(foreign_keyring)
    except (FileNotFoundError, ValueError):
        pass
    else:
        return

    if not _exchange_foreign_key():
        raise SystemExit(1)


def _exchange_foreign_key() -> bool:
    from . import keymgmt

    localkey = _load_or_generate_machine_key().convert_to_public()
    try:
        foreignkeys = asyncssh.read_public_key_list(foreign_keyring)
    except FileNotFoundError:
        foreignkeys = []

    newkey = keymgmt.exchange_foreign_key(localkey)
    if newkey is None:
        logger.info("No key received, aborting key exchange")
        return False
    elif newkey in foreignkeys:
        logger.info("Received key is already in the keyring, not adding")
        return False
    else:
        logger.info("Adding new key to keyring")
        newkey.append_public_key(foreign_keyring)
        return True


class _ReverseServer(asyncssh.SSHServer):
    __keyring: cabc.Sequence[asyncssh.SSHKey]

    def connection_made(self, conn: asyncssh.SSHServerConnection) -> None:
        conn.set_keepalive(5, 3)

    def begin_auth(self, _: str) -> bool:
        self.__keyring = asyncssh.read_public_key_list(foreign_keyring)
        return True

    def auth_completed(self) -> None:
        del self.__keyring

    def public_key_auth_supported(self) -> bool:
        return True

    def validate_public_key(self, _: str, key: asyncssh.SSHKey) -> bool:
        return key in self.__keyring

    def connection_requested(self, *_: str | int) -> bool:
        return True


class _ReverseClient(asyncssh.SSHClient):
    __conn: asyncssh.SSHClientConnection
    __key: asyncssh.SSHKey | None
    __keyring: cabc.Sequence[asyncssh.SSHKey]

    def __init__(self, tracker: ConnectionTracker) -> None:
        self.__tracker = tracker
        self.__key = _load_or_generate_machine_key()
        self.__keyring = asyncssh.read_public_key_list(foreign_keyring)

    def connection_made(self, conn: asyncssh.SSHClientConnection) -> None:
        self.__conn = conn
        self.__tracker.reconnect(conn)

        conn.set_keepalive(5, 3)

    def connection_lost(self, exc: Exception | None) -> None:
        if self.__conn is self.__tracker.connection:
            self.__tracker.reconnect(None)

    def validate_host_public_key(
        self, _1, _2, _3, key: asyncssh.SSHKey
    ) -> bool:
        return key in self.__keyring

    def public_key_auth_requested(self) -> asyncssh.SSHKey | None:
        key, self.__key = self.__key, None
        return key


async def _open_proxy(
    conn: asyncssh.SSHClientConnection | None,
    *,
    listen: str,
    port: int,
    local_forward: cabc.Iterable[ForwardSpec],
) -> None:
    if conn is None:
        return

    for spec in local_forward:
        await conn.forward_local_port(**dataclasses.asdict(spec))
    await conn.forward_socks(listen, port)


class ExecCallback:
    def __init__(self, cmd) -> None:
        self.__cmd = shlex.split(cmd)
        self.__connected = False

    async def on_reconnect(self, conn: asyncssh.SSHClientConnection | None) -> None:
        match (self.__connected, conn is not None):
            case (False, True):
                arg = "connected"
            case (True, False):
                arg = "disconnected"
            case (True, True):
                arg = "reconnected"
            case _:
                return

        self.__connected = conn is not None
        await anyio.run_process([*self.__cmd, arg])


if __name__ == "__main__":
    main(_anyio_backend="asyncio")
