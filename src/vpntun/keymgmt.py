"""The foreign key management UI."""
from __future__ import annotations

import tkinter as tk
from tkinter import ttk

import asyncssh  # type: ignore[import]


def exchange_foreign_key(
    localkey: asyncssh.SSHKey,
) -> asyncssh.SSHKey | None:
    """Start the UI to exchange a single foreign key.

    Args:
        localkey: The SSH public key of this side.
    """
    exchange = _KeyExchangeGUI(localkey)
    return exchange()


class _KeyExchangeGUI:
    localkey: asyncssh.SSHKey
    foreignkey: asyncssh.SSHKey | None

    __root: tk.Tk
    __foreignkey_var: tk.StringVar

    def __init__(self, localkey: asyncssh.SSHKey) -> None:
        self.localkey = localkey
        self.foreignkey = None

    def __call__(self) -> asyncssh.SSHKey | None:
        self.__root = root = tk.Tk()
        root.grid_columnconfigure(0, weight=1)

        localkey_var = tk.StringVar()
        localkey_var.set(
            self.localkey.export_public_key().decode("utf-8").strip()
        )
        localkey_entry = ttk.Entry(root, textvariable=localkey_var)
        localkey_label = ttk.Label(
            root, text="Paste the following text into the other side:"
        )

        self.__foreignkey_var = tk.StringVar()
        foreignkey_entry = ttk.Entry(root, textvariable=self.__foreignkey_var)
        foreignkey_label = ttk.Label(
            root, text="Paste the text from the other side here:"
        )

        ok_button = ttk.Button(root, text="OK", command=self.validate_key)

        localkey_label.grid(column=0, row=0, sticky="w")
        localkey_entry.grid(column=0, row=1, sticky="we")
        foreignkey_label.grid(column=0, row=2, sticky="w")
        foreignkey_entry.grid(column=0, row=3, sticky="we")
        ok_button.grid(column=0, row=4, sticky="e")

        root.mainloop()

        del self.__root, self.__foreignkey_var

        return self.foreignkey

    def validate_key(self) -> None:
        try:
            self.foreignkey = asyncssh.import_public_key(
                self.__foreignkey_var.get().strip().encode("utf-8")
            )
        except ValueError:
            self.foreignkey = None
        else:
            self.__root.destroy()
